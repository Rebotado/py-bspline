from scipy.interpolate import BSpline
import matplotlib.pyplot as plt
import numpy as np

p = 3
u = [-2, -2, -2, -2, -1, 0, 1, 2, 2, 2, 2]
c = [0, 0, 0, 6, 0, 0, 0]

spline = BSpline(u, c, p)
fig, ax = plt.subplots()
x_coordinates = np.linspace(-2, 2, 50)

ax.plot(x_coordinates, spline(x_coordinates))
plt.show()