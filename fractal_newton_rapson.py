from PIL import Image
imgx = 600
imgy = 400
image = Image.new("RGB", (imgx, imgy))

#Fuente: https://python-catalin.blogspot.com/2013/02/make-newton-fractal-with-python.html

# Area del fractal
xa = -2.0
xb = 2.0
ya = -2.0
yb = 2.0


max_iterations = 10 
step_derivat = 0.002e-1 
error = 5e-19 

# Funcion para generar el fractal
f = lambda z: z * z  + complex(-0.835,0.2321)

# Impresion del fractal
for y in range(imgy):
 zy = y * (yb - ya)/(imgy - 1) + ya
 for x in range(imgx):
  zx = x * (xb - xa)/(imgx - 1) + xa
  z = complex(zx, zy)
  for i in range(max_iterations):
   # make complex numerical derivative
   dz = (f(z + complex(step_derivat, step_derivat)) - f(z))/complex(step_derivat,step_derivat)
 # Newton iteration see wikipedia   
   z0 = z - f(z)/dz 
   # stop to the error 
   if abs(z0 - z) < error: 
    break
   z = z0
  image.putpixel((x, y), (i % 8 * 16, i % 4 * 32,i % 2 * 64))
  
image.save("fractal.png", "PNG")