from PIL import Image
import math

#Fuente: https://python-catalin.blogspot.com/2013/02/make-newton-fractal-with-python.html
#La formula original para general los fractales fue sacado de la fuente indicada
#Se cambio la programacion lineal a programacion orientada a objetos y se 
#cambio el uso de la funcion f(z) a uso de lambdas como parametros

#python 3.8.3

class Fractal:

    def __init__(self, f, 
    max_iterations=10, step_derivative = 0.002e-1, 
    error = 5e-19, xa = -2.0, xb = 2.0, ya = -2.0, 
    yb = 2.0,imgx = 600, imgy = 400):
        self.image = Image.new("RGB", (imgx, imgy))
        self.imgx = imgx
        self.imgy = imgy
        self.max_iterations = max_iterations
        self.step_derivative = step_derivative
        self.error = error
        self.f = f
        self.xa = xa
        self.xb = xb
        self.ya = ya
        self.yb = yb

    def generate_fractal(self):
        for y in range(self.imgy):
            zy = y * (self.yb - self.ya)/(self.imgy - 1) + self.ya
            for x in range(self.imgx):
                zx = x * (self.xb - self.xa)/(self.imgx - 1) + self.xa
                z = complex(zx, zy)
                for i in range(self.max_iterations):
                    # make complex numerical derivative
                    dz = (self.f(z + complex(self.step_derivative, self.step_derivative)) - self.f(z))/complex(self.step_derivative,self.step_derivative)
                    # Newton iteration see wikipedia   
                    z0 = z - self.f(z)/dz 
                    # stop to the error 
                    if abs(z0 - z) < self.error: 
                        break
                    z = z0
                self.image.putpixel((x, y), (i % 8 * 16, i % 4 * 32,i % 2 * 64))
        self.image.save("fractal.png", "PNG")


fractal = Fractal(lambda z: pow(z, 3) - 1, max_iterations=20) #La funcion que generara el fractal es obligatoria. El resto de los parametros es opcional.
fractal.generate_fractal()